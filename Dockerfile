FROM java:8
ADD HolaMundo.jar HolaMundo.jar
EXPOSE 8080
CMD ["java", "-jar", "HolaMundo.jar"]
